# API proxy

Proxy para desarrollar front local con CORE-DEV y KANTO-DEV.

Por default realiza el proxy de todas las peticiones a dev, pero es posible indicar que peticiones serán consumidas 
localmente.

Por default escucha en  http://localhost:8088, el puerto puede ser modificado en el archivo `.env` cambiando el valor de
 la variable `PORT` y reiniciando el servidor.

### CORE
Antes de realizar cualquier petición es necesario realizar el proceso de autenticación, se puede hacer a travez de la 
ruta: `POST` http://localhost:8088/auth/login, los parámetros necesarios son los mismos de la lambda de autenticación.

La cookie devuelta será almacenada en cache y será reutilizada, sin importar que se reinicie el servidor, a menos que 
caduque.

Para realizar logout es necesario llamar a la ruta: `GET` http://localhost:8088/auth/logout

#### Levantar proxy
`make exec`

#### Configurar rutas para consumo local
Para especificar las rutas que serán consumidas localmente es necesario crear un archivo json en el mismo directorio del 
ejecutable `main`, bajo el nombre `core-local-routes.json`. El formato del archivo es el siguiente:

```json
[
    {
        "enabled": true,
        "url": "/loans/{loanId}",
        "methods": ["GET"]
    }
]
``` 

Es posible configurar sólo alguno(s) metodos de la ruta, estos deben especificarse en la propiedad `methods`. Si el 
atributo `methods` es un arreglo vacio o no está presente, será procesado por defecto solamente el metodo `GET`
 
Es necesario reiniciar el servidor para que sean aplicados los cambios realizados en `core-local-routes.json`

#### Devolver un mock para una ruta
Para mockear una ruta solo se tiene que añadir el atributo `mock`, el cual es un arreglo que consta de 3 atributos; 
`method`, `status` y `data` los cuales sirven para especificar el metodo que se va a mockear, el código http de la 
respuesta y el objeto json de la misma

```json
[
    {
        "enabled": true,
        "url": "/loans/{loanId}",
        "methods": ["GET"],
        "mocks": [
            {
                "method": "GET",
                "status": 200,
                "data": {
                    "data": {
                        "myAttribute": "Hello world"
                    },
                    "time": "2021-01-19 12:49:02.417949"
                }
            }
        ]
    }
]
``` 

